This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app). For a detailed documentation, please visit the official Create React App [Readme](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md) section.

## How to run the app
1. `cd path/to/project`
2. Install dependencies (`npm i` or `yarn install`)
3. Run the project:
  * Launch the mock API (`npm run api` or `yarn api`)
  * Launch the app in development mode (`npm run start` or `yarn start`)

## Running tests and typechecking w/ flow
1. Runing flow: `npm run flow` or `yarn flow`
2. Runing tests: `npm run test` or `yarn test`

## Test coverage
I have provided tests only for a few files, that - in my opinion - were the most interesting to test. Those files are:
1. `src/api/index.js`
2. `src/components/App.js`
3. `src/components/provider.js`

## API
I have used [Dyson](http://webpro.github.io/dyson/) to mock API server. I have set the API server to return the response with a delay of `2000ms`. The config files for the mock API can be found in the `mocks` directory at the root level of the project.
