const Chance = require('chance');
const _ = require('lodash');

const chance = new Chance();

const latMinMax = { max: 52.675454, min:  52.338234 };
const lngMinMax = { max: 13.761117, min:  13.088346 };

const streets = [
  'Am Buchhorst (Arthur-Scheunert-Allee/L78)',
  'Potsdam-Brauhausberg - Friedrich-Engels-Straße (Heinrich-Mann-Allee/L78)',
  'Breite Straße - Feuerbachstraße (Zeppelinstraße/B2)',
  'Schopenhauerstraße (Hegelallee/B2)',
  'Gehsenerstraße - Mittelheide (Mahlsdorfer Straße/L1152)',
  'Seelenbinderstraße (Bahnhofstraße/L1152)',
  'Paul-Schneider-Straße - Leonorenstraße (Kaiser-Wilhelm-Straße/L1092)',
  'Blockdammweg (Rummelsburger Landstraße/L1075)',
  'Treskowallee - Edisonstraße (Rummelsburger Straße/L1075)',
];

const incidents = {
  path: '/incidents',
  collection: true,
  method: 'GET',
  cache: false,
  delay: 2000,
  size: () => _.random(3, 10),
  template: {
    "id": () => chance.guid(),
    "type": () => _.sample([1, 2, 3]),
    "point": {
      "x": () => chance.latitude(latMinMax),
      "y": () => chance.longitude(lngMinMax),
    },
    "from": () => _.sample(streets),
    "to": () => _.sample(streets),
    "details": () => chance.sentence(),
    "delay": () => _.random(100, 2000),
    "magnitude": () => _.sample([1, 2, 3, 4]),
  },
};

module.exports = incidents;
