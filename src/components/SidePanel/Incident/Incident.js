// @flow

import React from 'react';
import classnames from 'classnames';
import IconFlag from 'react-icons/lib/md/flag';
import IconPlace from 'react-icons/lib/md/place';

import {
  INCIDENT_TYPE,
  INCIDENT_MAGNITUDE,
} from '../../../utils/constants';
import type { IncidentType } from '../../../utils/types';

import './Incident.css';

type PropsType = {
  incident: IncidentType,
  showDetails: ?boolean,
};

/**
 * Focuses the selected incident element, to make sure
 * that the selected incident is visible on an overflowed panel.
 * This is useful when the incident was selected by clicking on a map marker.
 */
const focusActiveElement = (element: ?HTMLElement, shouldFocus: ?boolean) => {
  if (element && shouldFocus) {
    element.focus();
  }
};

const getHeadingLine = (icon, content) => (
  <h4 className="Incident__headingLine">{icon}{content}</h4>
);

const Incident = ({ incident, showDetails }: PropsType) => {
  const {
    id,
    type,
    from,
    to,
    details,
    delay,
    magnitude,
    point: { x, y },
  } = incident;

  const className = classnames('Incident', {'isSelected': showDetails});

  return (
    <article
      tabIndex="0"
      className={className}
      ref={(node) => { focusActiveElement(node, showDetails); }}
    >
      <header className="Incident__header">
        { getHeadingLine(<IconPlace className="Incident__headingLineIcon" />, from) }
        { getHeadingLine(<IconFlag className="Incident__headingLineIcon" />, to) }
      </header>
      <dl className="Incident__details">
        <dt className="Incident__detailsKey">Delay:</dt>
        <dd className="Incident__detailsValue">{delay}</dd>
        <dt className="Incident__detailsKey">ID:</dt>
        <dd className="Incident__detailsValue">{id}</dd>
        <dt className="Incident__detailsKey">Type:</dt>
        <dd className="Incident__detailsValue">{INCIDENT_TYPE[type]}</dd>
        <dt className="Incident__detailsKey">Location:</dt>
        <dd className="Incident__detailsValue">{`${x}, ${y}`}</dd>
        <dt className="Incident__detailsKey">Details:</dt>
        <dd className="Incident__detailsValue">{details}</dd>
        <dt className="Incident__detailsKey">Magnitude:</dt>
        <dd className="Incident__detailsValue">{INCIDENT_MAGNITUDE[magnitude]}</dd>
      </dl>
    </article>
  );
};

Incident.defaultProps = {
  incident: {},
  showDetails: false,
};

export default Incident;
