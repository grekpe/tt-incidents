// @flow

import React from 'react';
import classnames from 'classnames';

import type { IncidentType } from '../../utils/types';

import './SidePanel.css';
import IncidentList from './IncidentList/IncidentList';

type PropTypes = {
  incidents: Array<IncidentType>,
  selected: string,
  showPanel: boolean,
  selectHandler: Function,
};

const SidePanel = (props: PropTypes) => {
  const {
    incidents,
    selected,
    showPanel,
    selectHandler
  } = props;

  const className = classnames('SidePanel', {'isActivated': showPanel});

  return (
    <div className={className}>
      <p>Click on an incident to toggle details</p>
      <IncidentList
        selected={selected}
        incidents={incidents}
        clickHandler={selectHandler}
      />
    </div>
  );
};

SidePanel.defaultProps = {
  incidents: [],
  selected: '',
  showPanel: false,
  selectHandler: () => {},
};

export default SidePanel;
