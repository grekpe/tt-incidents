// @flow

import React from 'react';

import Incident from '../Incident/Incident';
import type { IncidentType } from '../../../utils/types';

import './IncidentList.css';

type PropsType = {
  incidents: Array<IncidentType>,
  selected: string,
  clickHandler: Function,
};

const IncidentList = ({ incidents, selected, clickHandler }: PropsType) => {
  if (incidents.length === 0) {
    return (
      <p>Today's a lucky day - seems there are no incidents!</p>
    );
  }
  return  (
    <ul className="IncidentList">
      {
        incidents.map((incident, i) => (
          <li
            key={i}
            className="IncidentList__item"
            onClick={() => { clickHandler(incident.id); }}
          >
            <Incident
              incident={incident}
              showDetails={incident.id === selected}
            />
          </li>
        ))
      }
    </ul>
  )
}

IncidentList.defaultProps = {
  incidents: [],
  selected: '',
  clickHandler: () => {},
};

export default IncidentList;
