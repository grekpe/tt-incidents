// @flow

import React from 'react';
import IconMenu from 'react-icons/lib/md/menu';

import Button from '../common/Button/Button';
import './Header.css';

type PropsType = {
  toggleHandler: Function,
}

const Header = ({ toggleHandler }: PropsType) => (
  <header className="Header">
    <Button
      onClick={toggleHandler}
      className="Button Header__button"
    >
      <IconMenu />
    </Button>
    <h1 className="Header__title">Incidents list</h1>
  </header>
);

Header.defaultProps = {
  toggleHandler: () => {}
}

export default Header;
