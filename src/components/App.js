// @flow

import React from 'react';

import {
  MAP_CENTER,
  MAP_ZOOM,
} from '../utils/constants';
import type { IncidentType } from '../utils/types';

import './App.css';
import Map from './Map/Map';
import Header from './Header/Header';
import SidePanel from './SidePanel/SidePanel';
import Loader from './common/Loader/Loader';

type PropsType = {
  allIncidents: Array<IncidentType>,
  currentIncident: string,
  isLoaderVisible: boolean,
  isSidePanelVisible: boolean,
  togglePanelHandler: Function,
  toggleIncidentHandler: Function,
};

/**
 * A simple, 'dumb', presentational component.
 * Makes things more reusable and much easier to test.
 * @param  {Object}  props  Component's props object.
 */
const App = (props: PropsType) => {
  const {
    allIncidents,
    currentIncident,
    isLoaderVisible,
    isSidePanelVisible,
    togglePanelHandler,
    toggleIncidentHandler,
  } = props;

  return (
    <div className="App">
      <Header
        toggleHandler={togglePanelHandler}
      />

      <div className="App__content">
        <Map
          zoom={MAP_ZOOM}
          center={MAP_CENTER}
          incidents={allIncidents}
          selected={currentIncident}
          clickHandler={toggleIncidentHandler}
        />

        <SidePanel
          incidents={allIncidents}
          selected={currentIncident}
          showPanel={isSidePanelVisible}
          selectHandler={toggleIncidentHandler}
        />
      </div>

      {isLoaderVisible && <Loader />}
    </div>
  );
}

App.defaultProps = {
  allIncidents: [],
  currentIncident: '',
  isLoaderVisible: false,
  isSidePanelVisible: false,
  togglePanelHandler: () => {},
  toggleIncidentHandler: () => {},
};

export default App;
