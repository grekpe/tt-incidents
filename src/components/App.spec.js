import React from 'react';
import renderer from 'react-test-renderer';
import Adapter from 'enzyme-adapter-react-16';
import { configure, shallow, mount } from 'enzyme';
import App from './App';

configure({ adapter: new Adapter() });

jest.mock('./Map/Map', () => (
  jest.fn(() => <div className="Map">A map component</div>)
));

const togglePanelMock = jest.fn(() => {});
const toggleIncidentMock = jest.fn(() => {});

const mockProps = {
  allIncidents: [{
    id: 'europe_HD_DE_TTL6000',
    type: 1,
    point: {
      x: 13.072789,
      y: 52.388527
    },
    from: 'Am Buchhorst (Arthur-Scheunert-Allee/L78)',
    to: 'Potsdam-Brauhausberg - Friedrich-Engels-Straße (Heinrich-Mann-Allee/L78)',
    details: 'stationary traffic',
    delay: 232,
    magnitude: 2
  }, {
    id: 'europe_HD_DE_TTL1211',
    type: 1,
    point: {
      x: 13.072789,
      y: 52.388527
    },
    from: 'Am Buchhorst (Arthur-Scheunert-Allee/L78)',
    to: 'Potsdam-Brauhausberg - Friedrich-Engels-Straße (Heinrich-Mann-Allee/L78)',
    details: 'stationary traffic',
    delay: 232,
    magnitude: 2
  }],
  currentIncident: '',
  isLoaderVisible: false,
  isSidePanelVisible: false,
  togglePanelHandler: togglePanelMock,
  toggleIncidentHandler: toggleIncidentMock,
};

describe('Given the App component', () => {
  afterEach(() => {
    togglePanelMock.mockReset();
    toggleIncidentMock.mockReset();
  });

  it('should render without crashing', () => {
    shallow(<App />);
  });

  it('should match a snapshot', () => {
    const component = renderer.create(<App />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should render with visible loader and match a snapshot', () => {
    const component = renderer.create(
      <App isLoaderVisible={true} />
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should render with full blown mock props and match a snapshot', () => {
    const component = renderer.create(
      <App {...mockProps} />
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should have the appropriate parts and call the appropriate functions', () => {
    const component = mount(
      <App {...mockProps} />
    );

    expect(component.find('.Map').length).toEqual(1);
    expect(component.find('.Header').length).toEqual(1);
    expect(component.find('.SidePanel').length).toEqual(1);
    expect(component.find('.SidePanel').hasClass('isActivated')).toBe(false);
    expect(component.find('.Incident').length).toEqual(2);
    component.find('.Incident').forEach((node) => {
      expect(node.hasClass('isSelected')).toBe(false);
    });

    // let's test some clicking:
    component.find('button.Header__button').simulate('click');
    expect(togglePanelMock).toHaveBeenCalled();
    component.find('.Incident').first().simulate('click');
    expect(toggleIncidentMock).toHaveBeenCalled();
  });
});
