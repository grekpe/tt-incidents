// @flow

import React, { PureComponent } from 'react';
import {
  Map as LeafletMap,
  ZoomControl,
  TileLayer,
} from 'react-leaflet';
import { LatLngBounds } from 'leaflet';

import Marker from './Marker';
import { MAP_CONFIG } from '../../utils/constants';
import type { IncidentType } from '../../utils/types';

import 'leaflet/dist/leaflet.css';
import './Map.css';

type PropType = {
  zoom: Array<number>,
  center: Array<number>,
  incidents: Array<IncidentType>,
  selected: string,
  clickHandler: Function,
};

class Map extends PureComponent<PropType> {
  map: ?{ leafletElement: Function };

  static defaultProps = {
    zoom: [],
    center: [],
    incidents: [],
    selected: '',
    clickHandler: () => {},
  };

  /**
   * Pans the map to a location.
   * @param {Array} latLng The location's lat/lng values.
   */
  panToLocation(latLng: Array<number>) {
    if (this.map) {
      const { leafletElement } = this.map;
      leafletElement.panTo(latLng);
    }
  }

  /**
   * Pans the map to the the active marker.
   * My first approach was to render the selected marker only when props changed.
   * This however resulted in somewhat jerky and unnatural behaviour. Therefore
   * I decided to utilize refs and Leaflet's 'panTo' method to enhance the UX.
   */
  componentDidUpdate() {
    const { selected, incidents } = this.props;

    if (selected) {
      const current = incidents
        .find(({ id }) => (id === selected));

      if (current) {
        const {
          point: { x, y }
        } = current;

        this.panToLocation([x, y]);
      }
    }
  }

  /**
   * @return JSX
   */
  render() {
    const { center, zoom, incidents, selected, clickHandler } = this.props;

    const mapProps: Object = {
      zoom,
      center,
      zoomControl: false,
      boundsOptions: {
        padding: [50, 50]
      },
    };

    // In my approach, to render the map I need:
    // a) A list of <Marker /> components to place on the map
    // b) An array of lat/lng values for the map to calc its bounds
    // For convenience, I am using Array.reduce() to get all thst info:
    const { markers, latLngArray } = incidents.reduce(
      (acc, incident, i) => {
        const { id, point: { x, y } } = incident;
        return {
          latLngArray: [ ...acc.latLngArray, [x, y] ],
          markers: [
            ...acc.markers,
            <Marker
              key={i}
              position={[x, y]}
              isActive={id === selected}
              onClick={() => { clickHandler(id); }}
            />,
          ],
        };
      },
      { markers: [], latLngArray: [] }
    );

    if (latLngArray.length) {
      mapProps.bounds = new LatLngBounds(latLngArray);
    }

    return (
      <LeafletMap
        className="Map"
        ref={node => { this.map = node; }}
        {...mapProps}
      >
        <TileLayer
          {...MAP_CONFIG}
        />
        <ZoomControl position="topright" />
        { markers }
      </LeafletMap>
    );
  }
}

export default Map;
