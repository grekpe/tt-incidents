// @flow

import React from 'react';
import { DivIcon, Point } from 'leaflet';
import { Marker as LeafletMarker } from 'react-leaflet';

const iconTpl = (isActive) => (
	`<div class="MarkerIcon${isActive ? ' isActive' : ''}">
		<div class="MarkerIcon__shadow"></div>
		<div class="MarkerIcon__pin"></div>
	</div>`
);

const iconDefault = new DivIcon({
	iconSize: new Point(32, 32),
	html: iconTpl(),
});

const iconActive = new DivIcon({
	iconSize: new Point(32, 32),
	html: iconTpl(true),
});

type PropType = {
	isActive: ?boolean,
};

const Marker = ({isActive, ...rest}: PropType) => (
  <LeafletMarker
		icon={isActive
			? iconActive
			: iconDefault}
		{...rest}
	/>
);

export default Marker;
