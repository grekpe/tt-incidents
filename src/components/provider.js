// @flow

import * as React from 'react';
import { PureComponent } from 'react';

import { compareByDelay } from '../utils/helpers';
import type { IncidentType } from '../utils/types';

type StateTypes = {
  incidents: Array<IncidentType>,
  selected: string,
  error: string,
  fetching: boolean,
  showingPanel: boolean,
};

type ArgTypes = {
  fetch: Function,
  wait: ?number,
};

/**
 * Returns the name of the wrapped component.
 * @param {ReactComponent} Component
 * @return {String} Components's name
 */
const getComponentName =
  (Component: React.ComponentType<any>) => (Component.displayName || Component.name || 'Component');

/**
 * HOC - used to handle the logic of the application and to keep track of the application
 * state, thus making other components stateless, presentational, more reusable and easier to test.
 *
 * @param {ReactComponent}  WrappedComponent
 * @param {function}        fetch
 */
function provider(WrappedComponent: React.ComponentType<any>, {fetch, wait}: ArgTypes): React.ComponentType<any> {
  class Provider extends PureComponent<Object, StateTypes> {
    timeout: number;
    togglePanel: Function;
    toggleIncident: Function;

    /**
     * @constructor
     */
    constructor(props: Object) {
      super(props);

      this.state = {
        incidents: [],
        selected: '',
        error: '',
        fetching: false,
        showingPanel: false,
      };

      this.togglePanel = this.toggleSidePanel.bind(this);
      this.toggleIncident = this.toggleIncidentDetails.bind(this);
    }

    /**
     * Toggles the incidents panel.
     */
    toggleSidePanel() {
      this.setState({
        showingPanel: !this.state.showingPanel,
      });
    }

    /**
     * Toggles an incident details.
     * @param {string} selected - Id of the selected incident.
     */
    toggleIncidentDetails(selected: string) {
      const { selected: current } = this.state;

      if (selected === current) {
        this.setState({
          selected: '',
        });
        return;
      }

      this.setState({
        selected,
        showingPanel: true,
      });
    }

    /**
     * Refreshes the component's data by calling an API endpoint.
     */
    async refreshData() {
      // let's tell the application
      // that we start fetching the data
      this.setState({
        fetching: true,
        error: '',
      });

      // just a simple convenience helper function
      // to keep the code DRY for setting the 'fetching' flag
      const handleApiResponse = (obj) => {
        this.setState({
          ...obj,
          fetching: false,
        });
      };

      try {
        handleApiResponse({
          incidents: (await fetch()).sort(compareByDelay),
        });
      }
      catch (error) {
        handleApiResponse({
          error
        });
      }
      finally {
        // finally, let's schedule the next API call...
        // I am using window.setTimeout instead of window.setInterval -
        // I am calling the API only after the previous request has finished,
        // so I don't have to worry about concurrent requests, or requests that take longer
        // than the interval (in case we wanted to decrease the delay).
        if (wait && wait > 0 && window) {
          this.timeout = window.setTimeout(() => {
            this.refreshData();
          }, wait);
        }
      }
    }

    /**
     * Starts data synchronization once the component's mounted.
     */
    componentDidMount() {
      this.refreshData();
    }

    /**
     * Clears the API call timeout when the component's get unmounted.
     */
    componentWillUnmount() {
      if (window && this.timeout) {
        window.clearTimeout(this.timeout);
      }
    }

    /**
    * @return {JSX}
    */
    render() {
      const { incidents, selected, fetching, showingPanel } = this.state;

      return (
        <WrappedComponent
          allIncidents={incidents}
          currentIncident={selected}
          isLoaderVisible={fetching}
          isSidePanelVisible={showingPanel}
          togglePanelHandler={this.togglePanel}
          toggleIncidentHandler={this.toggleIncident}
        />
      );
    }
  }

  Provider.displayName = `Provider(${getComponentName(WrappedComponent)})`;

  return Provider;
}

export default provider;
