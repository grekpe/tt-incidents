import React from 'react';

import logo from '../../../assets/logo.svg';
import './Loader.css';

/**
 * Loader class.
 */
const Loader = () => (
  <div className="Loader">
    <img src={logo} className="Loader__spinner" alt="loader" />
  </div>
);

export default Loader;
