// @flow

import React from 'react';
import './Button.css';

/**
 * A simple button component.
 */
const Button = (props: Object) => (<button {...props}>{props.children}</button>);

Button.defaultProps = {
  className: 'Button',
  type: 'button',
};

export default Button;
