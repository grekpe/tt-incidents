import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { configure, mount } from 'enzyme';
import provider from './provider';

configure({ adapter: new Adapter() });

const AppMock = jest.fn(() => <div className="App">A map component</div>);

describe('Given the provider HOC', () => {
  it('should call the componentDidMount method', async () => {
    const promise = Promise.resolve([]);
    const Provider = provider(AppMock, {
      fetch: () => promise,
    });
    const spy = jest.spyOn(Provider.prototype, 'componentDidMount');
    mount(<Provider />);
    await promise;
    expect(spy).toHaveBeenCalled();
  });
  it('should call the fetch function', async () => {
    expect.assertions(1);
    const promise = Promise.resolve([]);
    const mockFetchFn = jest.fn(() => promise);
    const Provider = provider(AppMock, {
      fetch: mockFetchFn,
    });
    mount(<Provider />);
    await promise;
    expect(mockFetchFn).toHaveBeenCalled();
  });
  it('should register the props on the wrapped component', async () => {
    expect.assertions(6);
    const promise = Promise.resolve([]);
    const Provider = provider(AppMock, {
      fetch: () => promise,
    });
    const wrapper = mount(<Provider />);
    await promise;
    wrapper.update();
    const component = wrapper.find(AppMock);
    expect(component.props().allIncidents).toEqual([]);
    expect(component.props().currentIncident).toEqual('');
    expect(component.props().isLoaderVisible).toBe(false);
    expect(component.props().isSidePanelVisible).toBe(false);
    expect(component.props().togglePanelHandler).toBeDefined();
    expect(component.props().toggleIncidentHandler).toBeDefined();
  });
  it('should update its state on a callback from the wrapped component', async () => {
    expect.assertions(1);
    const promise = Promise.resolve([]);
    const Provider = provider(AppMock, {
      fetch: () => promise,
    });
    const wrapper = mount(<Provider />);
    await promise;

    // let's call the 'togglePanelHandler' first
    wrapper.find(AppMock).props().togglePanelHandler();

    // let's make sure the state has updated
    wrapper.update();

    // side panel should be visible
    expect(wrapper.find(AppMock).props().isSidePanelVisible).toBe(true);
  });
});
