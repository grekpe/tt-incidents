import React from 'react';
import ReactDOM from 'react-dom';

import './index.css';
import App from './components/App';
import provider from './components/provider';
import fetchIncidents from './api';
import {
  REFRESH_DELAY
} from './utils/constants';

// let's create a 'container' component,
// responsible for fetching data and keeping app's state
const Provider = provider(App, {
  fetch: fetchIncidents,
  wait: REFRESH_DELAY
});

ReactDOM.render(<Provider />, document.getElementById('root'));
