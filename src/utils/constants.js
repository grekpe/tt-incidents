/**
 * The delay after which to call the incidents API endpoint
 */
export const REFRESH_DELAY = 2 * 60 * 1000;

/**
 *
 */
export const INCIDENT_TYPE = {
  '1': 'Traffic jam',
  '2': 'Dangerous conditions',
  '3': 'Road construction',
};

/**
 *
 */
export const INCIDENT_MAGNITUDE = {
  '1': 'Unknown',
  '2': 'Minor',
  '3': 'Moderate',
  '4': 'Severe',
};

/**
 *
 */
export const MAP_ZOOM = 13;

/**
 *
 */
export const MAP_CENTER = [52.5200, 13.4050];

/**
 * Map config object
 */
export const MAP_CONFIG = {
  attribution: '<a href="http://osm.org/copyright">OpenStreetMap</a> | <a href="http://mapbox.com">Mapbox</a>',
  accessToken: 'pk.eyJ1IjoiZ3Jla3BlIiwiYSI6ImNqOHptMzRsbjI2a3QzM3IwbTV4b3U4cTEifQ.YiEI3pZ_aJr_SJWOT8zmCw',
  url: "https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}",
  id: 'mapbox.streets',
}
