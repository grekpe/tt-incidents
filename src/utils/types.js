/* @flow */

/**
 * Incident object type
 */
export type IncidentType = {
  id: string,
  type: number,
  point: {
    x: number,
    y: number,
  },
  from: string,
  to: string,
  details: string,
  delay: number,
  magnitude: number,
};
