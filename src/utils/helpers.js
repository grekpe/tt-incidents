// @flow

import type { IncidentType } from './types';

/**
 * Helper comparator function
 */
export const compareByDelay =
  ({ delay: delayA }: IncidentType, { delay: delayB }: IncidentType): number => (delayB - delayA);
