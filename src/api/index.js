// @flow

import 'whatwg-fetch';

const makeApiCall = (endpoint: string): Promise<any> => {
  if (!endpoint) {
    throw new Error('API endpoint required!');
  }

  // let's do the actual api call and return a promise
  return fetch(endpoint)
    .then(response => {
      if (response.ok) {
        // the fetch was successfull
        // https://developer.mozilla.org/en-US/docs/Web/API/Body/json
        return response.json();
      }

      // reject with some meaningful reason
      return Promise.reject('There was a problem fetching the requested data.');
    });
}

const fetchIncidents = () => makeApiCall('/incidents');

export default fetchIncidents;
