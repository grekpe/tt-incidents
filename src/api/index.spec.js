import fetchIncidents from './index';

const response = {
  success: true,
};
const failedFetchMock = jest.fn(() => Promise.resolve({ ok: false }));
const successfulFetchMock = jest.fn(() => Promise.resolve({
  ok: true,
  json: () => {
    return response;
  },
}));

describe('API', () => {
  describe('given a failed request', () => {
    beforeEach(() => {
      window.fetch = failedFetchMock;
    });
    it('should throw an error', async () => {
      expect.assertions(1);
      try {
        await fetchIncidents();
      } catch (err) {
        expect(err).toEqual(
          'There was a problem fetching the requested data.'
        );
      }
    });
  });
  describe('given a successful request', () => {
    beforeEach(() => {
      window.fetch = successfulFetchMock;
    });
    it('should call the appropriate enpoint and have correct response', async () => {
      expect.assertions(3);
      const response = await fetchIncidents();
      expect(successfulFetchMock).toBeCalled();
      expect(successfulFetchMock).toBeCalledWith('/incidents');
      expect(response).toEqual(response);
    });
  });
});
